package com.young.mbgdiy.plugin;

import org.mybatis.generator.api.FullyQualifiedTable;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.internal.util.StringUtility;

import java.util.Date;
import java.util.List;

/**
 * TODO
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/5
 */
public class EntityCommentPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        addModelClassComment(topLevelClass, introspectedTable);
        return super.modelBaseRecordClassGenerated(topLevelClass, introspectedTable);
    }

    @Override
    public boolean modelRecordWithBLOBsClassGenerated(TopLevelClass topLevelClass,
                                                      IntrospectedTable introspectedTable) {

        addModelClassComment(topLevelClass, introspectedTable);
        return super.modelRecordWithBLOBsClassGenerated(topLevelClass, introspectedTable);
    }

    protected void addModelClassComment(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

        FullyQualifiedTable table = introspectedTable.getFullyQualifiedTable();
        String tableComment = introspectedTable.getRemarks();

        topLevelClass.addJavaDocLine("/**");
        if (StringUtility.stringHasValue(tableComment)) {
            topLevelClass.addJavaDocLine(" * " + tableComment + "<p/>");
        }
        topLevelClass.addJavaDocLine(" * " + table.toString() + "<p/>");
        topLevelClass.addJavaDocLine(" * @date " + new Date().toString());
        topLevelClass.addJavaDocLine(" *");
        topLevelClass.addJavaDocLine(" */");
    }
}
