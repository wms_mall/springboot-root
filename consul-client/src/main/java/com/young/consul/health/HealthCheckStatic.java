package com.young.consul.health;

/**
 * 健康监测测试实验共享数据
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/12/18
 */
public class HealthCheckStatic {
    /**
     * 健康检查计数器
     */
    public static volatile long checkCount = 0;
}
