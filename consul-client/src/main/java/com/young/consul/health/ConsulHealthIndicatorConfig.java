package com.young.consul.health;

import com.ecwid.consul.v1.ConsulClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.health.ConditionalOnEnabledHealthIndicator;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.consul.ConsulEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义Consul健康检查
 * spring.cloud.consul.discovery.health-check-path属性不配置的情况下
 * 就会调用Consul自己检查机制
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/12/2
 */
@Configuration
@ConditionalOnClass(Endpoint.class)
@AutoConfigureAfter(ConsulClient.class)
@AutoConfigureBefore(ConsulEndpoint.class)
public class ConsulHealthIndicatorConfig {


    @Value("${check.threshold:1000}")
    private long threshold;
    /**
     * 生效原理
     *
     * @param consulClient
     * @return
     * @see {@link org.springframework.boot.actuate.autoconfigure.health.HealthIndicatorRegistryBeans#get}
     */
    @Bean
    @ConditionalOnEnabledHealthIndicator("consul")
    public ConsulHealthIndicatorOverride consulHealthIndicator(ConsulClient consulClient) {
        return new ConsulHealthIndicatorOverride(consulClient,threshold);
    }
}
