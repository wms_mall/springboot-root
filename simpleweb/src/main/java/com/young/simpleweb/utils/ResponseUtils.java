package com.young.simpleweb.utils;

import com.alibaba.fastjson.JSON;
import com.young.common.frame.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Response处理工具
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/3/28
 */
public class ResponseUtils {
    private static Logger logger = LogManager.getLogger(ResponseUtils.class);

    /**
     * 返回结果统一数据格式
     *
     * @param response
     * @param result
     */
    public static void responseResult(HttpServletResponse response, Result result) {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-type", "application/json;charset=UTF-8");
        response.setStatus(200);
        try {
            response.getWriter().write(JSON.toJSONString(result));
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }
}
