package com.young.simpleweb.controller;

import com.young.common.entity.User;
import okhttp3.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * OkHttpClient
 * Http模拟请求测试
 */
public class OkHttpClientTest {
    String AuthorizationInfo;
    User userInfo;
    @Before
    public void setUp() {
        AuthorizationInfo="key_AuthorizationInfo";
        userInfo = new User();
        userInfo.setSex("male");
        userInfo.setName("young");
    }

    @After
    public void clearUp() {
        // todo
        // 释放资源等操作
    }

    /**
     * 表单形式参数
     *
     * @throws IOException
     */
    @Test
    public void formTest() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("AuthorizationInfo", AuthorizationInfo)
                .addFormDataPart("user", userInfo.toString())
                .build();
        Request request = new Request.Builder()
                .url("http://127.0.0.1:9531/simpleWeb/simplePost")
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        Response response = client.newCall(request).execute();
        System.out.print(response.body().string());
    }

    /**
     * application/x-www-form-urlencoded
     * 参数类型测试
     *
     * @throws IOException
     */
    @Test
    public void JsonTest() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "paramB=valueB&paramC=valueC");
        Request request = new Request.Builder()
                .url("http://127.0.0.1:9531/simpleWeb/simplePost")
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        Response response = client.newCall(request).execute();
        System.out.print(response.body().string());
    }
}