package com.young.consumer.controller;

import com.young.consumer.util.FileDemo;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
public class RestTempleteControllerTest {

    @Test
    public void mutilpartFile() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        RestTemplate client = new RestTemplate();
        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        body.add("file", FileDemo.getTestFile());
        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);
        String serverUrl = "http://127.0.0.1:9528/app/mutilpartFile";
        ResponseEntity<String> response = client
                .postForEntity(serverUrl, requestEntity, String.class);
    }
}