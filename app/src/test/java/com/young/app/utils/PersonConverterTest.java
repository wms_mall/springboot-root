package com.young.app.utils;

import com.alibaba.fastjson.JSON;
import com.young.app.dto.PersonDTO;
import com.young.app.mapper.PersonConverter;
import com.young.app.model.Gender;
import com.young.app.model.HomeAddress;
import com.young.app.pojo.PersonDO;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * mapStruct 测试用例
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/8/10
 */
@SpringBootTest
public class PersonConverterTest {
    @Test
    public void mapStructTest() {
        PersonDO doToDto = new PersonDO();
        doToDto.setName("DO->DTO");
        doToDto.setAge(26);
        doToDto.setBirthday(new Date());
        doToDto.setId(1);
        doToDto.setGender(Gender.MALE.name());
        HomeAddress homeAddress = new HomeAddress();
        homeAddress.setStreet("兴化街道");
        homeAddress.setNumber("108");
        doToDto.setAddress(JSON.toJSONString(homeAddress));
        PersonDTO personDTO = PersonConverter.INSTANCE.do2dto(doToDto);
        System.out.println("DO转DTO:\n" + personDTO);
        PersonDO dtoTodo = PersonConverter.INSTANCE.dto2do(personDTO);
        dtoTodo.setName("DTO->DO");
        System.out.println("DTO转DO:\n" + dtoTodo);
    }
}
