package com.young.app.dto;

import com.young.app.model.Gender;
import com.young.app.model.HomeAddress;

import java.util.Date;

/**
 * DTO demo
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/8/10
 */
public class PersonDTO {
    private String userName;

    private Integer age;

    private Date birthday;

    private Gender gender;

    private HomeAddress address;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public HomeAddress getAddress() {
        return address;
    }

    public void setAddress(HomeAddress address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "PersonDTO{" +
                "userName='" + userName + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                ", gender=" + gender +
                ", address=" + address +
                '}';
    }
}
