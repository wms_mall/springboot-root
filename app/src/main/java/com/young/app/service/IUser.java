package com.young.app.service;

import com.young.app.model.User;

import java.util.List;

/**
 * User处理方法
 *
 * @author ：young
 * @date ：Created in 2020/3/10
 */
public interface IUser {
    public boolean saveData(List<User> users);
}
