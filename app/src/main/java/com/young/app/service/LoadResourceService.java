package com.young.app.service;

import java.io.IOException;

/**
 * 加载Resource路径下的文件
 * 使用资源文件
 */
public interface LoadResourceService {
    void getResourceFile(String dir, String name) throws IOException;
    void getResourceFile2(String dir, String name) throws IOException;
    void getResourceFile3(String dir, String name) throws IOException;
    void getResourceFile4(String dir, String name) throws IOException;

}
