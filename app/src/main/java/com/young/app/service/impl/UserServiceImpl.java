package com.young.app.service.impl;

import com.alibaba.fastjson.JSON;
import com.sun.javafx.binding.StringFormatter;
import com.young.app.model.User;
import com.young.app.service.IUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO
 *
 * @author ：young
 * @date ：Created in 2020/3/10
 */
@Service
public class UserServiceImpl implements IUser {
    private static Logger logger = LogManager.getLogger(UserServiceImpl.class);

    /**
     * 得到一份元数据，需要做的操作
     *
     * @param users
     * @return
     */
    @Override
    public boolean saveData(List<User> users) {
        logger.info(StringFormatter.format("{%d}条数据，开始存储数据库！\n", users.size()).getValueSafe());
        logger.info(JSON.toJSONString(users));
        logger.info("UserService 存储数据库成功！");
        return true;
    }
}
