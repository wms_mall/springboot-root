package com.young.app.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：young
 * @date ：Created in 2019/7/8
 * @description：WebClient
 */
@RestController
@RequestMapping(value = "/app")
public class WebClientController {
    private static Logger logger = LogManager.getLogger(WebClientController.class);

    @RequestMapping(value = "/webClientGet", method = RequestMethod.GET)
    public String webClientGet() {
        return "webClientGet";
    }
    @RequestMapping(value = "/webClientPost", method = RequestMethod.POST)
    public String webClientPost() {
        return "webClientPost";
    }
    @RequestMapping(value = "/webClientMutilpartFile", method = RequestMethod.POST)
    public String webClientMutilpartFile(@RequestPart("file") FilePart filePart) {
        logger.info("uploadFile filename={}", filePart.filename());
        return filePart.filename();
    }
}
