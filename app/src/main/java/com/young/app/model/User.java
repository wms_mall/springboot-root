package com.young.app.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.young.app.converter.GenderConverter;

/**
 * Excel导入数据结构
 *
 * @author ：young
 * @date ：Created in 2020/3/10
 */
public class User {

    @ExcelProperty(value = "姓名",index = 0)
    private String name;

    @ExcelProperty(value = "年龄",index = 1)
    private Integer age;

    @ExcelProperty(value = "性别",index = 2, converter = GenderConverter.class)
    private Integer gender;

    @ExcelProperty(value = "生日",index = 3)
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private String birth;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }
}