package com.young.rocketmq.simpleexample.consumer;

import com.young.common.utils.TimeUtils;
import com.young.rocketmq.config.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 * Simple Message Example
 * Consumer
 * 简单示例 消费者通用
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/3/29
 */
public class Consumer {
    private static Logger logger = LogManager.getLogger(Consumer.class);

    public static void main(String[] args) throws  MQClientException {

        // Instantiate with specified consumer group name.
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(Const.GROUP);

        // Specify name server addresses.
        consumer.setNamesrvAddr(Const.NAME_SRV);

        // Subscribe one more more topics to consume.
        consumer.subscribe(Const.TOPIC, "*");
        // Register callback to execute on arrival of messages fetched from brokers.
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
                                                            ConsumeConcurrentlyContext context) {
                logger.info(String.format("%s Receive New Messages: %s %n", Thread.currentThread().getName(), msgs));
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        //Launch the consumer instance.
        consumer.start();
        logger.info("Consumer Started.%n");
        // 半小时后退出程序
        TimeUtils.sleep(180000);
        consumer.shutdown();
        return;
    }
}
