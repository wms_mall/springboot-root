package com.young.rocketmq.simpleexample.utils;

import com.alibaba.fastjson.JSON;
import com.young.rocketmq.config.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.admin.TopicStatsTable;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.apache.rocketmq.tools.admin.DefaultMQAdminExt;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RocketMQToolsTest {
    private static Logger logger = LogManager.getLogger(RocketMQToolsTest.class);

    @Test
    public void createTopicTest() {
        DefaultMQAdminExt defaultMQAdminExt = new DefaultMQAdminExt();
        defaultMQAdminExt.setAdminExtGroup(Const.GROUP);
        defaultMQAdminExt.setNamesrvAddr(Const.NAME_SRV);
        try {
            defaultMQAdminExt.start();
            String newTopic = defaultMQAdminExt.getAdminExtGroup();
            String brokerName = "rocketmq";
            defaultMQAdminExt.createTopic(brokerName, newTopic, 1);
            // 稍等片刻
            Thread.sleep(3000);
            TopicStatsTable topicStatsTable = defaultMQAdminExt.examineTopicStats(newTopic);
            Assert.isTrue(!ObjectUtils.isEmpty(topicStatsTable), "Topic不存在");
            logger.info(JSON.toJSON(topicStatsTable));
        } catch (MQClientException e) {
            logger.error(e.getErrorMessage());
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        } catch (RemotingException e) {
            logger.error(e.getMessage());
        } catch (MQBrokerException e) {
            logger.error(e.getErrorMessage());
        } finally {
            defaultMQAdminExt.shutdown();
        }
    }
}