/*** copyright (c) 2020 young  ***/
package com.young.mybatisdiy.service.impl;

import com.young.mbgdiy.base.BaseServiceImpl;
import com.young.mybatisdiy.mapper.MbgDiyMapper;
import com.young.mybatisdiy.model.MbgDiy;
import com.young.mybatisdiy.model.MbgDiyExample;
import com.young.mybatisdiy.service.MbgDiyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MbgDiyServiceImpl extends BaseServiceImpl<MbgDiy, MbgDiyExample, String> implements MbgDiyService {
    
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(MbgDiyServiceImpl.class);

    @Autowired
    public MbgDiyMapper mbgDiyMapper;

    @Autowired
    public void setMapper() {
        super.setMapper(mbgDiyMapper);
    }
}