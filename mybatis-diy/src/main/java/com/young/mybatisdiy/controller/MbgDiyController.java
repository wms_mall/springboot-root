package com.young.mybatisdiy.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.young.mybatisdiy.model.MbgDiy;
import com.young.mybatisdiy.model.MbgDiyExample;
import com.young.mybatisdiy.service.MbgDiyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author ：young
 * @date ：Created in 2020/04/13
 * @description： 数据库交互demo
 */
@RestController
@RequestMapping(value = "mybatis")
public class MbgDiyController {
    @Autowired
    MbgDiyService mbgDiyService;

    @RequestMapping(value = "pageHelper")
    @ResponseBody
    public PageInfo<MbgDiy> pageHelper(HttpServletRequest request, HttpServletResponse response) {
        MbgDiyExample con = new MbgDiyExample();
        // 开启分页，默认查询总数count
        PageHelper.startPage(1, 10);
        // 紧跟着的一个select方法会被分页
        List<MbgDiy> mbgDiyList = mbgDiyService.selectByExample(con);
        PageInfo<MbgDiy> page = new PageInfo(mbgDiyList);
        // 之后的select就不会再开启分页
        List<MbgDiy> disablePage = mbgDiyService.selectByExample(con);
        return page;
    }

    /**
     * 自定义实现分页
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "diyPage")
    @ResponseBody
    public com.young.mbgdiy.base.PageInfo<MbgDiy> diyPage(HttpServletRequest request, HttpServletResponse response) {
        MbgDiyExample con = new MbgDiyExample();
        // 紧跟着的一个select方法会被分页
        com.young.mbgdiy.base.PageInfo<MbgDiy> pageInfo= mbgDiyService.selectByPageExample(con, 1,10);
        return pageInfo;
    }
}
