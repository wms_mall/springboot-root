package com.young.swagger2jmeter.jmeter;

/**
 * JMeter测试计划
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2021/3/20
 */
import java.io.FileWriter;
public class TestPlan {
    private String filePath;
    private Session[] oSessions;
    public TestPlan(Session[] oSessions, String filePath) {
        this.filePath = filePath;
        this.oSessions = oSessions;
    }
    public String generateContent() throws Exception {
        if (this.oSessions.length == 0) {
            System.out.println("Error, There has no sessions, please check.");
            throw new Exception();
        }
        Session session = this.oSessions[0];
        String ip = session.getHostname();
        String port = session.getPort();
        String encoding = "utf-8";
        String protocol = session.getProtocol();
        Element element = new Element();
        // 基础配置
        String content = element.addConfigTestElement(null, ip, port, encoding, protocol, "HTTP请求默认值");
        // Cookie管理
        content = element.addCookieManager(content, "HTTP Cookie 管理器");
        // 用户自定义参数
        content = element.addArguments(content, "用户定义的变量");
        String path = null;
        StringBuilder builder = new StringBuilder();
        for (Session session2 : this.oSessions) {
            String content_tmp = element.addJSONPathAssertion(null, "$.stat", "OK", "验证响应结果");
            path = session2.getRequestPath();
            //ip
            ip = ip.equals(session2.getHostname()) ? "" : session2.getHostname();
            //port
            port = port.equals(session2.getPort()) ? "" : session2.getPort();
            //protocol
            protocol = protocol.equals(session2.getProtocol()) ? "" : session2.getProtocol();
            String requestBody = null;
            if (this.isExistContentType(session2)) {
                String contentType = session2.getContentType();
                if ((((!contentType.contains("boundary") && !contentType.contains("octet - stream")) && (!contentType.contains("image") && !contentType.contains("video"))) && ((!contentType.contains("audio") && !contentType.contains("tar")) && (!contentType.contains("zip") && !contentType.contains("rtf")))) && ((!contentType.contains("pdf") && !contentType.contains("powerpoint")) && (!contentType.contains("x-compress") && !contentType.contains("msword")))) {
                    requestBody = session2.getRequestBody();
                } else {
                    requestBody = "";
                }
            } else {
                requestBody = session2.getRequestBody();
            }
            if (path.contains("/unode/stor/uploadPart")) {
                requestBody = "";
            }
            content_tmp = element.surroundByHTTPSamplerProxy(content_tmp, ip, port, protocol, session2.getRequestMethod(), path, requestBody, path);
            builder.append(content_tmp);
        }
        String threadGroup = element.surroundByThreadGroup(builder.toString(), "线程组");
        StringBuilder builder2 = new StringBuilder();
        builder2.append(content);
        builder2.append(threadGroup);
        content = element.addViewResultTree(builder2.toString(), "察看结果树");
        content = element.addAssertionResult(content, "断言结果");
        content = element.surroundByTestPlan(content, "测试计划");
        content =element.addXmlHead(new XmlFormatter().format(content));
        System.out.println(content);
        return content;
    }
    private Boolean isExistContentType(Session session) {
        return session.getContextType() != null && session.getContextType().trim().length() > 0;
    }
    public void saveAsJMeterScript() throws Exception {
        String content = this.generateContent();
        FileWriter writer = new FileWriter(filePath, true);
        try {
            writer.write(content);
            System.out.println("Jmeter脚本导出成功 !");
        }
        finally {
            writer.close();
        }
    }
}