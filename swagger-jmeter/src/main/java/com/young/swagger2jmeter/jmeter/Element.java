package com.young.swagger2jmeter.jmeter;

/**
 * JMeter脚本元素类
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2021/3/20
 */
public class Element {
    private StringBuilder stringBuilder;

    private Boolean blankStr(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        return true;
    }

    public String addArguments(String content, String elementName) {
        this.stringBuilder = new StringBuilder();
        if (blankStr(content)) {
            this.stringBuilder.append(content);
        }
        this.stringBuilder.append(String.format("<Arguments guiclass=\"ArgumentsPanel\" testclass=\"Arguments\" testname=\"%s\" enabled=\"true\">", elementName));
        this.stringBuilder.append("<collectionProp name=\"Arguments.arguments\"/>");
        this.stringBuilder.append("</Arguments>");
        this.stringBuilder.append("<hashTree/>");
        return this.stringBuilder.toString();
    }

    public String addAssertionResult(String content, String elementName) {
        this.stringBuilder = new StringBuilder();
        if (blankStr(content)) {
            this.stringBuilder.append(content);
        }
        this.stringBuilder.append(String.format("<ResultCollector guiclass=\"AssertionVisualizer\" testclass=\"ResultCollector\" testname=\"%s\" enabled=\"true\">", elementName));
        this.stringBuilder.append("<boolProp name=\"ResultCollector.error_loggin\">false</boolProp>");
        this.stringBuilder.append("<objProp>");
        this.stringBuilder.append("<name>saveConfig</name>");
        this.stringBuilder.append("<value class=\"SampleSaveConfiguration\">");
        this.stringBuilder.append("<time>true</time>");
        this.stringBuilder.append("<latency>true</latency>");
        this.stringBuilder.append("<timestamp>true</timestamp>");
        this.stringBuilder.append("<success>true</success>");
        this.stringBuilder.append("<label>true</label>");
        this.stringBuilder.append("<code>true</code>");
        this.stringBuilder.append("<message>true</message>");
        this.stringBuilder.append("<threadName>true</threadName>");
        this.stringBuilder.append("<dataType>true</dataType>");
        this.stringBuilder.append("<encoding>false</encoding>");
        this.stringBuilder.append("<assertions>true</assertions>");
        this.stringBuilder.append("<subresults>true</subresults>");
        this.stringBuilder.append("<responseData>false</responseData>");
        this.stringBuilder.append("<samplerData>false</samplerData>");
        this.stringBuilder.append("<xml>false</xml>");
        this.stringBuilder.append("<fieldNames>true</fieldNames>");
        this.stringBuilder.append("<responseHeaders>false</responseHeaders>");
        this.stringBuilder.append("<requestHeaders>false</requestHeaders>");
        this.stringBuilder.append("<responseDataOnError>false</responseDataOnError>");
        this.stringBuilder.append("<saveAssertionResultsFailureMessage>true</saveAssertionResultsFailureMessage>");
        this.stringBuilder.append("<assertionsResultsToSave>0</assertionsResultsToSave>");
        this.stringBuilder.append("<bytes>true</bytes>");
        this.stringBuilder.append("<sentBytes>true</sentBytes>");
        this.stringBuilder.append("<threadCounts>true</threadCounts>");
        this.stringBuilder.append("<idleTime>true</idleTime>");
        this.stringBuilder.append("<connectTime>true</connectTime>");
        this.stringBuilder.append("</value>");
        this.stringBuilder.append("</objProp>");
        this.stringBuilder.append("<stringProp name=\"filename\"></stringProp>");
        this.stringBuilder.append("</ResultCollector>");
        this.stringBuilder.append("<hashTree/>");
        return this.stringBuilder.toString();
    }

    public String addConfigTestElement(String content, String ip, String port, String encoding, String protocol, String elementName) {
        this.stringBuilder = new StringBuilder();
        if (blankStr(content)) {
            this.stringBuilder.append(content);
        }
        this.stringBuilder.append(String.format("<ConfigTestElement guiclass=\"HttpDefaultsGui\" testclass=\"ConfigTestElement\" testname = \"%s\" enabled = \"true\">", elementName));
        this.stringBuilder.append("<elementProp name=\"HTTPsampler.Arguments\" elementType=\"Arguments\" guiclass=\"HTTPArgumentsPanel\" testclass=\"Arguments\" testname=\"用户定义的变量\" enabled=\"true\">");
        this.stringBuilder.append("<collectionProp name=\"Arguments.arguments\"/>");
        this.stringBuilder.append("</elementProp>");
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.domain\">%s</stringProp>", ip));
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.port\">%s</stringProp>", port));
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.protocol\">%s</stringProp>", protocol));
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.contentEncoding\">%s</stringProp>", encoding));
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.path\"></stringProp>");
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.concurrentPool\">6</stringProp>");
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.connect_timeout\"></stringProp>");
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.response_timeout\"></stringProp>");
        this.stringBuilder.append("</ConfigTestElement>");
        this.stringBuilder.append("<hashTree/>");
        return this.stringBuilder.toString();
    }

    public String addCookieManager(String content, String elementName) {
        this.stringBuilder = new StringBuilder();
        if (blankStr(content)) {
            this.stringBuilder.append(content);
        }
        this.stringBuilder.append(String.format("<CookieManager guiclass=\"CookiePanel\" testclass=\"CookieManager\" testname=\"%s\" enabled=\"true\">", elementName));
        this.stringBuilder.append("<collectionProp name=\"CookieManager.cookies\"/>");
        this.stringBuilder.append("<boolProp name=\"CookieManager.clearEachIteration\">false</boolProp>");
        this.stringBuilder.append("</CookieManager>");
        this.stringBuilder.append("<hashTree/>");
        return this.stringBuilder.toString();
    }

    public String addJSONPathAssertion(String content, String jsonPath, String expectedValue, String elementName) {
        this.stringBuilder = new StringBuilder();
        if (blankStr(content)) {
            this.stringBuilder.append(content);
        }
        this.stringBuilder.append(String.format("<com.atlantbh.jmeter.plugins.jsonutils.jsonpathassertion.JSONPathAssertion guiclass = \"com.atlantbh.jmeter.plugins.jsonutils.jsonpathassertion.gui.JSONPathAssertionGui\" testclass=\"com.atlantbh.jmeter.plugins.jsonutils.jsonpathassertion.JSONPathAssertion\" testname=\"%s\" enabled=\"true\">", elementName));
        this.stringBuilder.append(String.format("<stringProp name=\"JSON_PATH\">%s</stringProp>", jsonPath));
        this.stringBuilder.append(String.format("<stringProp name=\"EXPECTED_VALUE\">%s</stringProp>", expectedValue));
        this.stringBuilder.append("<boolProp name=\"JSONVALIDATION\">true</boolProp>");
        this.stringBuilder.append("<boolProp name=\"EXPECT_NULL\">false</boolProp>");
        this.stringBuilder.append("<boolProp name=\"INVERT\">false</boolProp>");
        this.stringBuilder.append("<boolProp name=\"ISREGEX\">true</boolProp>");
        this.stringBuilder.append("</com.atlantbh.jmeter.plugins.jsonutils.jsonpathassertion.JSONPathAssertion>");
        this.stringBuilder.append("<hashTree/>");
        return this.stringBuilder.toString();
    }

    public String addViewResultTree(String content, String elementName) {
        this.stringBuilder = new StringBuilder();
        if (blankStr(content)) {
            this.stringBuilder.append(content);
        }
        this.stringBuilder.append(String.format("<ResultCollector guiclass = \"ViewResultsFullVisualizer\" testclass=\"ResultCollector\" testname=\"%s\" enabled=\"true\">", elementName));
        this.stringBuilder.append("<boolProp name=\"ResultCollector.error_logging\">false</boolProp>");
        this.stringBuilder.append("<objProp>");
        this.stringBuilder.append("<name>saveConfig</name>");
        this.stringBuilder.append("<value class=\"SampleSaveConfiguration\">");
        this.stringBuilder.append("<time>true</time>");
        this.stringBuilder.append("<latency>true</latency>");
        this.stringBuilder.append("<timestamp>true</timestamp>");
        this.stringBuilder.append("<success>true</success>");
        this.stringBuilder.append("<idleTime>true</idleTime>");
        this.stringBuilder.append("<connectTime>true</connectTime>");
        this.stringBuilder.append("</value>");
        this.stringBuilder.append("</objProp>");
        this.stringBuilder.append("<stringProp name=\"filename\"></stringProp>");
        this.stringBuilder.append("</ResultCollector>");
        this.stringBuilder.append("<hashTree/>");
        return this.stringBuilder.toString();
    }

    public String addXmlHead(String xmlBody) {
        this.stringBuilder = new StringBuilder();
        //this.stringBuilder.append("<?xml version="1.0" encoding="UTF-8"?>n"); 
        this.stringBuilder.append(xmlBody);
        return this.stringBuilder.toString();
    }

    public String surroundByHTTPSamplerProxy(String content, String ip, String port, String protocol, String method, String path, String value, String elementName) {
        this.stringBuilder = new StringBuilder();
        this.stringBuilder.append(String.format("<HTTPSamplerProxy guiclass=\"HttpTestSampleGui\" testclass=\"HTTPSamplerProxy\" testname=\"%s\" enabled=\"true\">", elementName));
        this.stringBuilder.append("<boolProp name=\"HTTPSampler.postBodyRaw\">true</boolProp>");
        this.stringBuilder.append("<elementProp name=\"HTTPsampler.Arguments\" elementType=\"Arguments\">");
        this.stringBuilder.append("<collectionProp name=\"Arguments.arguments\">");
        this.stringBuilder.append("<elementProp name=\"\" elementType=\"HTTPArgument\">");
        this.stringBuilder.append("<boolProp name=\"HTTPArgument.always_encode\">false</boolProp>");
        this.stringBuilder.append(String.format("<stringProp name=\"Argument.value\">%s</stringProp>", value));
        this.stringBuilder.append("<stringProp name=\"Argument.metadata\">=</stringProp>");
        this.stringBuilder.append("</elementProp>");
        this.stringBuilder.append("</collectionProp>");
        this.stringBuilder.append("</elementProp>");
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.domain\">%s</stringProp>", ip));
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.port\">%s</stringProp>", port));
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.protocol\">%s</stringProp>", protocol));
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.contentEncoding\"></stringProp>");
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.path\">%s</stringProp>", path));
        this.stringBuilder.append(String.format("<stringProp name=\"HTTPSampler.method\">%s</stringProp>", method));
        this.stringBuilder.append("<boolProp name=\"HTTPSampler.follow_redirects\">true</boolProp>");
        this.stringBuilder.append("<boolProp name=\"HTTPSampler.auto_redirects\">false</boolProp>");
        this.stringBuilder.append("<boolProp name=\"HTTPSampler.use_keepalive\">true</boolProp>");
        this.stringBuilder.append("<boolProp name=\"HTTPSampler.DO_MULTIPART_POST\">false</boolProp>");
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.embedded_url_re\"></stringProp>");
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.connect_timeout\"></stringProp>");
        this.stringBuilder.append("<stringProp name=\"HTTPSampler.response_timeout\"></stringProp>");
        this.stringBuilder.append("</HTTPSamplerProxy>");
        this.stringBuilder.append("<hashTree>");
        this.stringBuilder.append(content);
        this.stringBuilder.append("</hashTree>");
        return this.stringBuilder.toString();
    }

    public String surroundByTestPlan(String content, String testPlanName) {
        this.stringBuilder = new StringBuilder();
        this.stringBuilder.append("<jmeterTestPlan version=\"1.2\" properties=\"3.2\">");
        this.stringBuilder.append("<hashTree>");
        this.stringBuilder.append(String.format("<TestPlan guiclass=\"TestPlanGui\" testclass=\"TestPlan\" testname=\"%s\" enabled=\"true\">", testPlanName));
        this.stringBuilder.append("<stringProp name=\"TestPlan.comments\"></stringProp>");
        this.stringBuilder.append("<boolProp name=\"TestPlan.functional_mode\">false</boolProp>");
        this.stringBuilder.append("<boolProp name=\"TestPlan.serialize_threadgroups\">false</boolProp>");
        this.stringBuilder.append("<elementProp name=\"TestPlan.user_defined_variables\" elementType=\"Arguments\" guiclass=\"ArgumentsPanel\" testclass=\"Arguments\" testname=\"用户定义的变量\" enabled=\"true\">");
        this.stringBuilder.append("<collectionProp name=\"Arguments.arguments\"/>");
        this.stringBuilder.append("</elementProp>");
        this.stringBuilder.append("<stringProp name=\"TestPlan.user_define_classpath\"></stringProp>");
        this.stringBuilder.append("</TestPlan>");
        this.stringBuilder.append("<hashTree>");
        if (blankStr(content)) {
            this.stringBuilder.append(content);
        }
        this.stringBuilder.append("</hashTree>");
        this.stringBuilder.append("<WorkBench guiclass=\"WorkBenchGui\" testclass=\"WorkBench\" testname=\"工作台\" enabled=\"true\">");
        this.stringBuilder.append("<boolProp name=\"WorkBench.save\">true</boolProp>");
        this.stringBuilder.append("</WorkBench>");
        this.stringBuilder.append("<hashTree/>");
        this.stringBuilder.append("</hashTree>");
        this.stringBuilder.append("</jmeterTestPlan>");
        return this.stringBuilder.toString();
    }

    public String surroundByThreadGroup(String content, String elementName) {
        this.stringBuilder = new StringBuilder();
        this.stringBuilder.append(String.format("<ThreadGroup guiclass=\"ThreadGroupGui\" testclass=\"ThreadGroup\" testname=\"%s\" enabled=\"true\">", elementName));
        this.stringBuilder.append("<stringProp name=\"ThreadGroup.on_sample_error\">continue</stringProp>");
        this.stringBuilder.append("<elementProp name=\"ThreadGroup.main_controller\" elementType=\"LoopController\" guiclass=\"LoopControlPanel\" testclass=\"LoopController\" testname=\"循环控制器\" enabled=\"true\">");
        this.stringBuilder.append("<boolProp name=\"LoopController.continue_forever\">false</boolProp>");
        this.stringBuilder.append("<stringProp name=\"LoopController.loops\">1</stringProp>");
        this.stringBuilder.append("</elementProp>");
        this.stringBuilder.append("<stringProp name=\"ThreadGroup.num_threads\">1</stringProp>");
        this.stringBuilder.append("<stringProp name=\"ThreadGroup.ramp_time\">1</stringProp>");
        long num = System.currentTimeMillis();
        this.stringBuilder.append(String.format("<longProp name=\"ThreadGroup.start_time\">%s</longProp>", num));
        this.stringBuilder.append(String.format("<longProp name=\"ThreadGroup.end_time\">%s</longProp>", num));
        this.stringBuilder.append("<boolProp name=\"ThreadGroup.scheduler\">false</boolProp>");
        this.stringBuilder.append("<stringProp name=\"ThreadGroup.duration\"></stringProp>");
        this.stringBuilder.append("<stringProp name=\"ThreadGroup.delay\"></stringProp>");
        this.stringBuilder.append("</ThreadGroup>");
        this.stringBuilder.append("<hashTree>");
        this.stringBuilder.append(content);
        this.stringBuilder.append("</hashTree>");
        return this.stringBuilder.toString();
    }
}
