package com.young.swagger2jmeter;

import com.young.swagger2jmeter.jmeter.JMeterExporter;
import com.young.swagger2jmeter.jmeter.Session;
import com.young.swagger2jmeter.utils.SpringApplicationContextUtil;
import io.swagger.models.Path;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.Parameter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import springfox.documentation.service.Documentation;
import springfox.documentation.spring.web.DocumentationCache;
import springfox.documentation.swagger2.mappers.ServiceModelToSwagger2Mapper;
import springfox.documentation.swagger2.web.Swagger2ControllerWebMvc;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 获取Swagger接口文档
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2021/3/20
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SwaggerDocumentTest {
    @Autowired
    Swagger2ControllerWebMvc swagger2ControllerWebMvc;
    @Autowired
    ServiceModelToSwagger2Mapper mapper;

    /**
     * 参考API{@link springfox.documentation.swagger2.web.Swagger2ControllerWebMvc#getDocumentation}     * 可以将接口文档拿到
     */
    @Test
    public void getApiDocumentBySwaggerTest() {
        DocumentationCache documentationCache = SpringApplicationContextUtil.getApplicationContext().getBean(DocumentationCache.class);
        Documentation documentation = documentationCache.documentationByGroup("default");
        if (documentation != null) {
            Swagger swagger = mapper.mapDocumentation(documentation);
            Map<String, Path> paths = swagger.getPaths();
            for (Map.Entry<String, Path> entry : paths.entrySet()) {
                Path value = entry.getValue();
                if (value.getPost() != null) {
                    String params = value.getPost().getParameters().stream().map(Parameter::getName).collect(Collectors.joining(",", "{", "}"));
                    System.out.printf("接口：%s，参数结构：%s", entry.getKey(), params);
                }
            }
            Assert.assertTrue(swagger != null);
        }

    }

    @Test
    public void swaggerTransferSession() {
        DocumentationCache documentationCache = SpringApplicationContextUtil.getApplicationContext().getBean(DocumentationCache.class);
        Documentation documentation = documentationCache.documentationByGroup("default");
        if (documentation != null) {
            Swagger swagger = mapper.mapDocumentation(documentation);
            Map<String, Path> paths = swagger.getPaths();
            ArrayList<Session> sessionList = new ArrayList<>(paths.size());
            for (Map.Entry<String, Path> entry : paths.entrySet()) {
                Path value = entry.getValue();
                Session session = new Session();
                session.setProtocol("http");
                session.setHostname("127.0.0.1");
                session.setPort("8080");
                session.setRequestPath(entry.getKey());
                session.setContentType("application/x-www-form-urlencoded");
                session.setContextType("application/x-www-form-urlencoded");
                if (value.getPost() != null) {
                    String params = value.getPost().getParameters().stream().map(Parameter::getName).collect(Collectors.joining(",", "{", "}"));
                    session.setRequestMethod("POST");
                    session.setRequestBody(params);
                }
                sessionList.add(session);
            }
            JMeterExporter exporter = new JMeterExporter();
            exporter.ExportSessions("./", sessionList.toArray(new Session[sessionList.size()]));
            Assert.assertTrue(swagger != null);
        }

    }


}
